import {Fragment, useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import OrderContext from '../OrderContext.js';
import CartDetails from './CartDetails.js';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import {Container, Row, Col, Form, Button, Table, Card} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CartOut(){

	const {user} = useContext(UserContext);
	const {order, setOrder, unSetOrder} = useContext(OrderContext);

	const [items, setItems] = useState([]);

	const [address, setAddress] = useState("");
	const [instructions, setInstructions] = useState("");

	useEffect(() => {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/myOrders`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(cart => {
			// console.log(cart);
			setItems(cart.map((e) => {
				console.log(e)
				return(
					<tbody>
						
						{e.ingredients.map((product, _id)=> {
							return(
								<tr>
									<td key = {_id}>{product.productName}</td>
									<td key = {_id}>{product.quantity}</td>
									<td key = {_id}>{product.subTotal}</td>
								</tr>
								)
						})}
						<hr/>
						<tr>
							<td>Order Total: {e.total}</td>
						</tr>
					</tbody>
					)
			}))

		})
		// .catch(err => console.log(err));
	}, []);

	// console.log(items);

	const navigate = useNavigate();

	function checkout() {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/checkout`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				address: address,
				instructions: instructions
			})
		})
		.then(res => res.json())
		.then(save => {

			if(save){

				unSetOrder();
				setOrder(null);
				// console.log(order);

				Swal.fire({
					title: "Thank you for ordering!",
					text: "Your order is now in the kitchen.",
					icon: "success"
				})

				navigate("/menu");

			} else {
				Swal.fire({
					title: "Checkout failed. Please try again!",
					icon: "error"
				})
			}
		})
	};


	return(

		
		<Fragment>
			{
				order?
				<Container>
					<Row>
						<Table>
							<thead>
								<tr>
									<th>Product Name</th>
									<th>Quantity</th>
									<th>Subtotal</th>
								</tr>
							</thead>
								{items}
							<Container className = "mt-4">
								{/*<Row>
									<Card>
									      <Card.Body>Order Total: {items.total}</Card.Body>
								    </Card>
								</Row>*/}
								<Row>
									<Container>
										<Form>
											<Form.Group className="mb-3" controlId="formBasicAddress">
											  <Form.Label>Address</Form.Label>
											  <Form.Control 
											  	type="string"
											  	placeholder="Enter address to deliver order to or 'pick-up'."
											  	value={address}
											  	onChange={(event)=>setAddress(event.target.value)}/>
											  <Form.Text className="text-muted">
											    We'll never share your personal information with anyone else.
											  </Form.Text>
											</Form.Group>
											<Form.Group className="mb-3" controlId="formBasicInstructs">
											  <Form.Label>Special Instructions</Form.Label>
											  <Form.Control 
											  	type="string" 
											  	placeholder='(e.g. "Ring doorbell," "No onions," etc.)'
											  	value={instructions}
											  	onChange={(event)=>setInstructions(event.target.value)} />
											</Form.Group>
										</Form>
									</Container>
								</Row>
								<Row>
									<Col>
									<Container>
											<Button className = "me-3" variant="success" onClick={() => checkout()}>
										        Confirm Order
										    </Button>
										    {/*<Button className = "me-3" variant="danger" onClick={() => clear()}>
										        Clear
										    </Button>*/}
										    <Button variant="info" as = {Link} to = "/menu">
										        Back to menu
										    </Button>
									</Container>
									</Col>
								</Row>
							</Container>
						</Table>
					</Row>
				</Container>				
				:
				<Container className = "text-center mt-5">
					<img className = "my-2" src = 'https://i.ibb.co/bX34Z2T/emptycart.jpg' alt="Empty cart image by Paul Seling." id = "emptycart"/>
					<h3 className = "mt-2">Your cart is empty!</h3>
					<h5 className = "text-muted">Fill it up <Link to = "/menu">here.</Link></h5>
				</Container>				
			}
		</Fragment>
		)
}