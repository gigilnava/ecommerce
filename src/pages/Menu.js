import MenuItems from '../components/MenuItems.js';
import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Row, Nav} from 'react-bootstrap';
import UserContext from '../UserContext.js';

export default function Menu() {

	const [items, setItems] = useState([]);

	// UseStates for conditional menus
	// true means hidden={true}
	/*const [hideAll, setHideAll] = useState(true);
	const [hideWok, setHideWork] = useState(true);
	const [hideTops, setHideTops] = useState(true);
	const [hideSauce, setHideSauce] = useState(true);*/

	// const {user} = useContext(UserContext);

	// For all menu items

	useEffect(() => {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/menu`)
		.then(result => result.json())
		.then(data => {
			// console.log(data);
			setItems(data.map(product => {
				return(
					<MenuItems key = {product._id} prodProp = {product}/>
					);
				})
			)
		})
		// .catch(err => console.log(err));
	}, []);

	// For only wok items

	function findWok(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/menu/wok`)
		.then(result=>result.json())
		.then(data => {
			setItems(data.map(wok => {
				return(
					<MenuItems key ={wok._id} prodProp = {wok}/>
					);
			}))
		})
		// .catch(err => console.log(err));
	};

	// For only toppings

	function findToppings(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/menu/toppings`)
		.then(result=>result.json())
		.then(data => {
			// console.log(data);
			setItems(data.map(tops => {
				return(
					<MenuItems key= {tops._id} prodProp = {tops}/>
					);
			}))
		})
		// .catch(err => console.log(err))
	};

	// For only sauce

	function findSauce(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/menu/sauce`)
		.then(result=>result.json())
		.then(data => {
			setItems(data.map(sauce => {
				return(
					<MenuItems key = {sauce._id} prodProp = {sauce}/>
					)
			}))
		})
		// .catch(err => console.log(err))
	};

	// Back to all items

	function findAll(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/menu`)
		.then(result => result.json())
		.then(data => {
			// console.log(data);
			setItems(data.map(product => {
				return(
					<MenuItems key = {product._id} prodProp = {product}/>
					);
				})
			)
		})
		// .catch(err => console.log(err));
	};

	return(
		<Fragment>
			<Container>
				<h1 className = "text-center">Menu</h1>
				<Row className = "my-3">
					<Nav justify variant="tabs" defaultActiveKey="/">
					      <Nav.Item>
					        <Nav.Link onClick={(event)=>findAll(event)}>All Items</Nav.Link>
					      </Nav.Item>
					      <Nav.Item>
					        <Nav.Link onClick={(event)=>findWok(event)}>Wok</Nav.Link>
					      </Nav.Item>
					      <Nav.Item>
					        <Nav.Link onClick={(event)=>findToppings(event)}>Toppings</Nav.Link>
					      </Nav.Item>
					      <Nav.Item>
					        <Nav.Link onClick={(event)=>findSauce(event)}>Sauce</Nav.Link>
					      </Nav.Item>
				    </Nav>
				</Row>
				<Row>
					{items}
				</Row>
			</Container>
		</Fragment>
		)
}