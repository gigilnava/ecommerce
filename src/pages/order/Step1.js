/*import {Fragment, useContext, useState, useEffect} from 'react';
import {Container, ProgressBar, Row, Col, Card, Button, Form, Modal} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext.js';
import OrderContext from '../../OrderContext.js';

export default function Step1 ({prodProp}) {

	const {_id, name, description, price, itemClass, image} = prodProp;

	const [quantity, setQuantity] = useState("");
	const [address, setAddress] = useState("");
	const [instructions, setInstructions] = useState("");

	const {user} = useContext(UserContext);
	const {order, setOrder} = useContext(OrderContext);

	const navigate = useNavigate();

	// UseStates for modal
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	function newOrder (event) {

		event.preventDefault();

		fetch(`http://localhost:4000/order/:prodId`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: quantity,
				address: address,
				instructions: instructions
			})
		})
		.then(result => result.json())
		.then(save => {

			if(save){
				Swal.fire({
					title: "Item added to cart!",
					icon: "success"
				})

				setOrder(save);
				
				// navigate("")
			} else {
				Swal.fire({
					title: "Ordering error.",
					text: "See console for more info.",
					icon: "error"
				})
			}
		})
	};



	return(
		user && user.isAdmin
		?
		navigate("*")
		:
		<Fragment>
			<Container>
				<Row>`
					<Col>
						<Card style={{ width: '18rem', height: '18rem'}} className = "mx-auto mt-2">
						      <Card.Img id = "user-menu-item-img" variant="top" src={image} className = "w-100"/>
						      <Card.Body>
						      	<Card.Title hidden>{_id}</Card.Title>
						        <Card.Title>{name} | {itemClass}</Card.Title>
						        <Card.Text id = "user-menu-price">
						          {price}
						        </Card.Text>
						        <Button variant="warning" onClick={() => setShow(true)}>
					                Select Size
					            </Button>
						      </Card.Body>
				    	</Card>
			    	</Col>
		    	</Row>
			</Container>

			<Modal
				show={show}
				cancel={show.close}
			    size="lg"
			    aria-labelledby="contained-modal-title-vcenter"
			    centered
			    onSubmit={(event)=>newOrder(event)}
			    >
			      <Modal.Header>
			        <Modal.Title id="contained-modal-title-vcenter">
			          {name}
			        </Modal.Title>
			      </Modal.Header>
			      <Modal.Body>
			      	<Row>
				        <h4>Description</h4>
				        <p>
				          {description}
				        </p>
				        <p>
				        PhP {price}
				        </p>
			        </Row>
			        <Row>
			        	<Form>
			        	      <Form.Group className="mb-3" controlId="formBasicQuantity">
			        	        <Form.Label>Quantity</Form.Label>
			        	        <Form.Control 
			        	        	type="number" 
			        	        	placeholder="Enter email"
			        	        	value={quantity}
			        	        	onChange={(event)=>setQuantity(event.target.value)} />
			        	      </Form.Group>

			        	      <Form.Group className="mb-3" controlId="formBasicAddress">
			        	        <Form.Label>Address</Form.Label>
			        	        <Form.Control 
			        	        	type="string"
			        	        	placeholder="Enter address to deliver order to or 'pick-up'."
			        	        	value={address}
			        	        	onChange={(event)=>setAddress(event.target.value)}/>
			        	        <Form.Text className="text-muted">
			        	          We'll never share your personal information with anyone else.
			        	        </Form.Text>
			        	      </Form.Group>
			        	      <Form.Group className="mb-3" controlId="formBasicInstructs">
			        	        <Form.Label>Special Instructions</Form.Label>
			        	        <Form.Control 
			        	        	type="string" 
			        	        	placeholder='(e.g. "Ring doorbell," "No onions," etc.)'
			        	        	value={instructions}
			        	        	onChange={(event)=>setInstructions(event.target.value)} />
			        	      </Form.Group>
			        	      <Button variant="primary" type="submit">
			        	        Submit
			        	      </Button>
		        	    </Form>
			        </Row>
			      </Modal.Body>
			      <Modal.Footer>
			      	<Button variant = "success" onClick={(event)=>setShow(false)} as = {Link} to = "/orderStart">Start Ordering</Button>
			        <Button variant = "secondary" onClick={(event)=>setShow(false)}>Close</Button>
			      </Modal.Footer>
			    </Modal>
		</Fragment>
		)
}*/