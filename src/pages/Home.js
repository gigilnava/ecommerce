import {Container, Row, Button} from 'react-bootstrap';
import {useNavigate, Link} from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel';
import {Fragment} from 'react';
import Img1 from '../images/storefront.png';
import Img2 from '../images/spread.png';
import Img3 from '../images/wokspread.png';

export default function Home () {

	const navigate = useNavigate();

	return(
		<Fragment>
			<Carousel id = "home-carousel" className = "d-none d-lg-block">
			      <Carousel.Item>
			        <img
			          className="d-block w-100"
			          src={Img1}
			          alt="First slide"
			          class = "carouselimg"
			        />
			      </Carousel.Item>

			      <Carousel.Item>
			        <img
			          className="d-block w-100"
			          src={Img2}
			          alt="Second slide"
			          class = "carouselimg"
			        />
			      </Carousel.Item>

			      <Carousel.Item>
			        <img
			          className="d-block w-100"
			          src={Img3}
			          alt="Third slide"
			          class = "carouselimg"
			        />
			      </Carousel.Item>
			    </Carousel>
		    <Carousel id = "home-carousel-2" className = "d-none d-md-block d-lg-none">
		          <Carousel.Item>
		            <img
		              className="d-block w-100"
		              src={Img1}
		              alt="First slide"
		              class = "carousel2img"
		            />
		          </Carousel.Item>

		          <Carousel.Item>
		            <img
		              className="d-block w-100"
		              src={Img2}
		              alt="Second slide"
		              class = "carousel2img"
		            />
		          </Carousel.Item>

		          <Carousel.Item>
		            <img
		              className="d-block w-100"
		              src={Img3}
		              alt="Third slide"
		              class = "carousel2img"
		            />
		          </Carousel.Item>
		        </Carousel>
			<Container className = "mt-4 mb-3" id = "home-bg">
						<Row>
							<Button variant="success" as = {Link} to = "/menu">Get your wok now!</Button>
						</Row>
						<Row className = "mt-3">
							<Button variant="secondary" href="https://www.asiaambgracia.com/web/" target="_blank">About Us</Button>
						</Row>
			</Container>
		</Fragment>
		)
}