import {Fragment, useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import OrderContext from '../OrderContext.js';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import {Container, Row, Col, Form, Button, Table, Card} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CartOut({orderProp}) {

	// const {_id, userId, ingredients, ingredients: {productName}, ingredients: {quantity}, ingredients: {subTotal}, status, total} = orderProp;

	const _id = orderProp._id;
	console.log(_id);

	const userId = orderProp.userId;
	const ingredients = orderProp.ingredients;

	const [items, setItems] = useState([]);

	let orderedItems = ingredients.map((order)=>{
		return(
			<tr key = {order._id}>
				<td>{order.productName}</td>
				<td>{order.quantity}</td>
				<td>{order.subTotal}</td>
			</tr>
			)
	});

	setItems(orderedItems);

	console.log(items);


	// let {productId, productName, quantity, subTotal} = orderedItems;

	const status = orderProp.status;
	const total = orderProp.total;

	/*const items = (orderProp) => {
	    const items = orderProp?. ingredients || []
	return items.map((item,i) => {
	        console.log(item);
	    })
	};*/

	// console.log(orderProp.ingredients);

	// const items = orderProp.ingredients;
	// console.log(items);

	// const {productName, quantity, subTotal} = items;
	
	// console.log(items.productName);

	// const [orderItems, setOrderItems] = useState([]);

	// setOrderItems(orderProp.ingredients);

	// console.log(orderItems);

	// const [productInfo, setProductInfo] = useState([]);

	/*orderProp.ingredients.map(data => {

		setProductInfo(data);
	});*/

	const {user} = useContext(UserContext);
	const {order, setOrder,unSetOrder} = useContext(OrderContext);

	const navigate = useNavigate();

	const [address, setAddress] = useState("");
	const [instructions, setInstructions] = useState("");

	// useEffect(()=>{

	// 	fetch(`http://localhost:4000/order/myOrders`,{
	// 		headers: {
	// 			Authorization: `Bearer ${localStorage.getItem('token')}`
	// 		}
	// 	})
	// 	.then(res => res.json())
	// 	.then()

	// }, [])

	function checkout() {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/checkout`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				address: address,
				instructions: instructions
			})
		})
		.then(res => res.json())
		.then(save => {

			if(save){

				unSetOrder();
				setOrder(null);
				// console.log(order);

				Swal.fire({
					title: "Thank you for ordering!",
					text: "Your order is now in the kitchen.",
					icon: "success"
				})

				navigate("/menu");

			} else {
				Swal.fire({
					title: "Checkout failed. Please try again!",
					icon: "error"
				})
			}
		})
	};

	function clear() {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/clear`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(res => {

			if(res){

				unSetOrder();

				/*useEffect(() => {
					unSetUser();
					setUser(null);
				}, []);*/

				Swal.fire({
					title: "Cart cleared.",
					icon: "success"
				})
				navigate("/menu")
			} else {
				Swal.fire({
					title: "Error. Please try again!",
					text: "See console for more info.",
					icon: "error"
				})
			}
		})
	};

	return(
		<Fragment>
			<tbody>
				{items}
			</tbody>
			<Container className = "mt-4">
				<Row>
					<Card>
					      {/*<Card.Body>Order Total: {total}</Card.Body>*/}
				    </Card>
				</Row>
				<Row>
					<Container>
						<Form>
							<Form.Group className="mb-3" controlId="formBasicAddress">
							  <Form.Label>Address</Form.Label>
							  <Form.Control 
							  	type="string"
							  	placeholder="Enter address to deliver order to or 'pick-up'."
							  	value={address}
							  	onChange={(event)=>setAddress(event.target.value)}/>
							  <Form.Text className="text-muted">
							    We'll never share your personal information with anyone else.
							  </Form.Text>
							</Form.Group>
							<Form.Group className="mb-3" controlId="formBasicInstructs">
							  <Form.Label>Special Instructions</Form.Label>
							  <Form.Control 
							  	type="string" 
							  	placeholder='(e.g. "Ring doorbell," "No onions," etc.)'
							  	value={instructions}
							  	onChange={(event)=>setInstructions(event.target.value)} />
							</Form.Group>
						</Form>
					</Container>
				</Row>
				<Row>
					<Col>
					<Container>
							<Button className = "me-3" variant="success" onClick={() => checkout()}>
						        Confirm Order
						    </Button>
						    {/*<Button className = "me-3" variant="danger" onClick={() => clear()}>
						        Clear
						    </Button>*/}
						    <Button variant="info" as = {Link} to = "/menu">
						        Back to menu
						    </Button>
					</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
		)
};