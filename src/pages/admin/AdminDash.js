import {Container, Row, Col, Button} from 'react-bootstrap';
import {Fragment, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom';
import UserContext from '../../UserContext.js';
import OrderViewer from './OrderViewer.js';
import {motion as m} from 'framer-motion';

export default function AdminDash () {

	const {user} = useContext(UserContext);

	return(

		user && user.isAdmin 
		?
		<m.Fragment 
		initial={{opacity: 0}}
		animate={{opacity: 1}}
		transition={{duration: 0.74, ease: "easeOut"}}>
			<Container className = "bg-img absolute top-0 left-0 w-full h-full">
				<Row className = "mt-4 mb-3">
					<h1 className = "text-center">Welcome, Admin!</h1>
				</Row>
				<Row className = "my-4">
					<Col className = "col-lg-6 order-lg-1 order-2 mt-3 mt-lg-0">
						<Container id = "order-box">
							<OrderViewer/>
						</Container>
					</Col>
					<Col id = "button-box" className = "col-lg-6 order-lg-2 order-1 d-flex flex-column align-items-center justify-content-center">
								<Button variant = "warning" className = "admin-dash-btns mb-2 text-muted" as = {Link} to = "/admin/allOrders">View Orders</Button>
								<Button variant = "secondary" className = "admin-dash-btns mb-2 text-white" as = {Link} to = "/admin/allProducts">Product Control</Button>
								<Button variant = "success" className = "admin-dash-btns" as = {Link} to = "/admin/create">Create New Product</Button>
					</Col>
				</Row>
			</Container>
		</m.Fragment>
		:
		<Navigate to = "*"/>
		)
}