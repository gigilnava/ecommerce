import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Row} from 'react-bootstrap';
import UserContext from '../../UserContext.js';
import {useNavigate} from 'react-router-dom';
import OrderItems from '../../components/admin/OrderItems.js';

export default function AllOrders () {

	const [orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/allOrders`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setOrders(data.map(order => {
				return(
					<OrderItems key = {order._id} orderProp = {order}/>
					)
			}))
		})
		// .catch(err => console.log(err));
	}, []);

	return(
		user && user.isAdmin
		?
		<Fragment>
		<Container>
			<Row>
				<h1 className = "text-center mt-3">All Orders</h1>
				{orders}
			</Row>
		</Container>
		</Fragment>
		:
		navigate("*")
		)
};