import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Nav, Row, Table} from 'react-bootstrap';
import UserContext from '../../UserContext.js';
import {useNavigate} from 'react-router-dom';
import ProdItems from '../../components/admin/ProdItems.js';
import Refresh from '../../images/arrow-rotate-right-solid.svg';

export default function AllOrders () {

	const [products, setProducts] = useState([]);
	
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/allMenu`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					<ProdItems key = {product._id} prodProp = {product}/>
					)
			}))
		})
		// .catch(err => console.log(err));
	}, []);

	function reFetch(event) {

		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/allMenu`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result=>result.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					<ProdItems key = {product._id} prodProp = {product}/>
					)
			}))
		})
		// .catch(err=>console.log(err))
	};

	// Functions for viewing products by category

	// For only wok items

	function findWok(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/menu/allWok`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result=>result.json())
		.then(data => {
			setProducts(data.map(wok => {
				return(
					<ProdItems key ={wok._id} prodProp = {wok}/>
					);
			}))
		})
		// .catch(err => console.log(err));
	};

	// For only toppings

		function findToppings(event){

			event.preventDefault();

			fetch(`${process.env.REACT_APP_CAPSTONE2}/product/menu/allTops`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(result=>result.json())
			.then(data => {
				console.log(data);
				setProducts(data.map(tops => {
					return(
						<ProdItems key= {tops._id} prodProp = {tops}/>
						);
				}))
			})
			// .catch(err => console.log(err))
		};

		// For only sauce

		function findSauce(event){

			event.preventDefault();

			fetch(`${process.env.REACT_APP_CAPSTONE2}/product/menu/allSauce`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(result=>result.json())
			.then(data => {
				setProducts(data.map(sauce => {
					return(
						<ProdItems key = {sauce._id} prodProp = {sauce}/>
						)
				}))
			})
			// .catch(err => console.log(err))
		};

		// Back to all items

		function findAll(event){

			event.preventDefault();

			fetch(`${process.env.REACT_APP_CAPSTONE2}/product/allMenu`)
			.then(result => result.json())
			.then(data => {
				// console.log(data);
				setProducts(data.map(product => {
					return(
						<ProdItems key = {product._id} prodProp = {product}/>
						);
					})
				)
			})
			// .catch(err => console.log(err));
		};

	return(
		user && user.isAdmin
		?
		<Fragment>
		<Container>
			<Row>
				<Row>
					<h1 className = "text-center mt-3">All Products</h1>
					<img id="refetch" src={Refresh} onClick={(event) => reFetch(event)} alt = "Refresh button."/>
				</Row>
				<Row className = "mb-3">
					<Nav justify variant="tabs" defaultActiveKey="/">
					      <Nav.Item>
					        <Nav.Link onClick={(event)=>findAll(event)}>All Items</Nav.Link>
					      </Nav.Item>
					      <Nav.Item>
					        <Nav.Link onClick={(event)=>findWok(event)}>Wok</Nav.Link>
					      </Nav.Item>
					      <Nav.Item>
					        <Nav.Link onClick={(event)=>findToppings(event)}>Toppings</Nav.Link>
					      </Nav.Item>
					      <Nav.Item>
					        <Nav.Link onClick={(event)=>findSauce(event)}>Sauce</Nav.Link>
					      </Nav.Item>
				    </Nav>
				</Row>
				<Table>
					<thead>
						<tr>
							<th>Product #</th>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Type</th>
							<th>Actions</th>
						</tr>
					</thead>
						{products}
				</Table>
			</Row>
			{/*<UpBtn/>*/}
		</Container>
		</Fragment>
		:
		navigate("*")
		)
};